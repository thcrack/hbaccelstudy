import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os
import csv
import statistics

postures = ['seated', 'standing']
directions = ['back', 'front', 'left', 'right']

folder_name = 'generated'

file_list = os.listdir(folder_name)

all_peaks = []

for posture in postures:
    for direction in directions:
        filtered_list = [k for k in file_list if posture in k and direction in k]
        cond_peaks = []
        for file in filtered_list:

            peak = -999999.0

            with open(folder_name + '\\' + file, newline='') as csvfile:

                rows = csv.reader(csvfile)

                next(rows, None)

                for row in rows:
                    a = float(row[1])
                    if a > peak:
                        peak = a

            all_peaks.append(peak)
            cond_peaks.append(peak)

        print('-----MEDIAN OF PEAKS ' + posture + '/' + direction + '-----')
        print(statistics.median(cond_peaks))

print('-----MEDIAN OF ALL PEAKS-----')
print(statistics.median(all_peaks))

print('-----MEAN OF ALL PEAKS-----')
print(statistics.mean(all_peaks))
print('STDEV: ' + str(statistics.stdev(all_peaks)))

for posture in postures:
    filtered_list = [k for k in file_list if posture in k]
    cond_peaks = []
    for file in filtered_list:

        peak = -999999.0

        with open(folder_name + '\\' + file, newline='') as csvfile:

            rows = csv.reader(csvfile)

            next(rows, None)

            for row in rows:
                a = float(row[1])
                if a > peak:
                    peak = a

        cond_peaks.append(peak)
        
    print('-----MEDIAN OF PEAKS ' + posture + '-----')
    print(statistics.median(cond_peaks))

for direction in directions:
    filtered_list = [k for k in file_list if direction in k]
    cond_peaks = []
    for file in filtered_list:

        peak = -999999.0

        with open(folder_name + '\\' + file, newline='') as csvfile:

            rows = csv.reader(csvfile)

            next(rows, None)

            for row in rows:
                a = float(row[1])
                if a > peak:
                    peak = a

        cond_peaks.append(peak)
        
    print('-----MEDIAN OF PEAKS ' + direction + '-----')
    print(statistics.median(cond_peaks))