import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os
import csv
import statistics

postures = ['seated', 'standing']
directions = ['back', 'front', 'left', 'right']

folder_name = 'generated'

file_list = os.listdir(folder_name)

def make_graph(file_list, posture, direction, ax):
    
    ax.set_xticks(np.arange(-.5, 6.5, .5))
    ax.axis([-.5, 6, 0, 50])
    ax.tick_params(axis="x", labelsize=15)
    ax.tick_params(axis="y", labelsize=15)
    #ax.axvspan(0, 3, facecolor='#84d070', alpha=0.2)
    ax.axvline(x=0, color='#84d070', alpha=.75, linewidth=3, dashes=((8,2,8,2)))
    ax.axvline(x=3, color='#84d070', alpha=.75, linewidth=3, dashes=((8,2,8,2)))
    ax.set_title(posture + ' - ' + direction, fontsize='x-large')
    ax.set_xlabel('time (s)', fontsize=20)
    ax.set_ylabel('angle difference (deg)', fontsize=20)
    ax.grid(alpha=.5)

    filtered_list = [k for k in file_list if posture in k and direction in k]
    peaks = []
    t_s = []
    a_s = []

    for file in filtered_list:

        t = []
        a = []

        with open(folder_name + '\\' + file, newline='') as csvfile:

            rows = csv.reader(csvfile)

            next(rows, None)

            for row in rows:
                t.append(float(row[0]))
                a.append(float(row[1]))

        t_s.append(t)
        a_s.append(a)

        ax.plot(t, a, color='#ff3c7e', alpha=.2, linewidth=2)
        peaks.append(max(a))

    # avg line
    avgx = np.linspace(-.5, 6, 1300)
    avgy = np.empty(len(avgx), dtype=float)
    mediany = []
    for xindex, avgxp in enumerate(avgx):
        yvals = []
        for i in range(len(t_s)):
            yval = np.interp(avgxp, t_s[i], a_s[i])
            avgy[xindex] += yval
            yvals.append(yval)
        mediany.append(statistics.median(yvals))
        avgy[xindex] /= len(t_s)
    
    # avg
    #ax.plot(avgx, avgy, color='#3c73e3', linewidth=2)
    ax.plot(avgx, mediany, color='#3c73e3', linewidth=3)
    
    for peak in peaks:
        ax.axhline(y=peak, xmax=0.025, color='#ff3c7e')

fig, axs = plt.subplots(2, 4)

for pidx, posture in enumerate(postures):
    for didx, direction in enumerate(directions):
        make_graph(file_list, posture, direction, axs[pidx, didx])

for ax in axs.flat:
    ax.label_outer()

params = plt.gcf()
plSize = params.get_size_inches()
params.set_size_inches( (plSize[0]*4, plSize[1]*2) )

plt.tight_layout()
plt.savefig('chartgen.png')
plt.show()