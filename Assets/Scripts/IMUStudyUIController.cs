﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AirDriVR
{
    public class IMUStudyUIController : MonoBehaviour
    {
        public Toggle seatedToggle;

        public InputField testerIdField;

        public Text mainButtonText;

        public Text eventDisplayText;

        public IMUStudyController imuStudyController;

        private void Start()
        {
            imuStudyController.onWriteEvent += OnWriteEvent;
            imuStudyController.onSessionEnd += OnSessionEnd;
        }

        private void OnWriteEvent(object sender, EventArgs e)
        {
            eventDisplayText.text = (sender as IMUStudyController)?.LastWriteEventDescription;
        }

        private void OnSessionBegin()
        {
            seatedToggle.interactable = testerIdField.interactable = false;
            mainButtonText.text = "End Session";
        }
        
        private void OnSessionEnd(object sender, EventArgs e)
        {
            seatedToggle.interactable = testerIdField.interactable = true;
            mainButtonText.text = "Start Session";
        }

        public void BeginOrEndStudy()
        {
            if (imuStudyController.isStudyOngoing)
            {
                imuStudyController.EndSession();
                return;
            }
        
            if (int.TryParse(testerIdField.text, out var testerId) && testerId < IMUStudyController.TesterCount)
            {
                imuStudyController.isSeated = seatedToggle.isOn;
                imuStudyController.testerId = testerId;
                imuStudyController.BeginStudy();
                OnSessionBegin();
            }
            else
            {
                Debug.LogError("Can't begin study: testerId Input field has invalid value (not integer or out of bound)");
            }
        }
    }
}