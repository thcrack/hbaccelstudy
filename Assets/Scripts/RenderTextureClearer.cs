﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace AirDriVR
{
    public class RenderTextureClearer : MonoBehaviour
    {
        public bool clearOnDisable = true;
        public RenderTexture renderTexture;

        [Button]
        public void Clear()
        {
            if(renderTexture) renderTexture.Release();
        }

        private void OnDisable()
        {
            if(clearOnDisable) Clear();
        }
    }
}