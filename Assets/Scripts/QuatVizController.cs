﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

[Serializable]
public struct IMUDataEntry
{
    public float time;
    public Quaternion quat;

    public IMUDataEntry(float time, Quaternion quat)
    {
        this.time = time;
        this.quat = quat;
    }
}

[Serializable]
public enum HBEventType
{
    SessionStart,
    SessionEnd,
    StartActuatingFront,
    StartActuatingBack,
    StartActuatingRight,
    StartActuatingLeft,
    StopActuating
}

[Serializable]
public struct HBEventEntry
{
    public float time;
    public HBEventType type;

    public HBEventEntry(float time, HBEventType type)
    {
        this.time = time;
        this.type = type;
    }
}

public class QuatVizController : MonoBehaviour
{
    [FilePath] public string eventPath;

    [FilePath] public string dataPath;

    [FolderPath] public string quatVizBatchFolderPath;
    
    [FolderPath] public string generatedAngleDiffFolderPath;

    [FilePath] public string eulerPath;
    
    [FilePath] public string angleDifferencePath;

    public Vector2Int fromToRange;

    public float lineThickness = 5;
    [Range(0,1)] public float lineAlpha = 0.1f;

    [HideInInspector] public List<Vector3> loadedRightVectors;
    [HideInInspector] public List<IMUDataEntry> loadedIMUData;
    [HideInInspector] public List<HBEventEntry> loadedEventData;

    [Button]
    public void Load()
    {
        Load(dataPath, eventPath);
    }
    
    private void Load(string inDataPath, string inEventPath)
    {
        loadedRightVectors = new List<Vector3>();
        loadedIMUData = new List<IMUDataEntry>();
        
        var reader = new StreamReader(inDataPath);
        reader.ReadLine(); // discard the first row

        while (!reader.EndOfStream)
        {
            var nums = reader.ReadLine().Split(',');
            var quat = new Quaternion(float.Parse(nums[5]), float.Parse(nums[6]), float.Parse(nums[7]), float.Parse(nums[4])).normalized;
            loadedRightVectors.Add(quat * Vector3.right);
            loadedIMUData.Add(new IMUDataEntry(float.Parse(nums[0]), quat));
        }
        
        reader.Close();
        reader.Dispose();
        
        loadedEventData = new List<HBEventEntry>();
        reader = new StreamReader(inEventPath);
        reader.ReadLine(); // discard the first row
        
        while (!reader.EndOfStream)
        {
            var nums = reader.ReadLine().Split(',');
            loadedEventData.Add(new HBEventEntry(float.Parse(nums[0]), ConvertStringToHBEventType(nums[1])));
        }
        
        reader.Close();
        reader.Dispose();
        
        Debug.Log($"Loaded data from {inDataPath} and {inEventPath}");
    }

    private static Dictionary<string, HBEventType> _hbEventTypeLookup = new Dictionary<string, HBEventType>()
        {
            {"SessionStart", HBEventType.SessionStart},
            {"SessionEnd", HBEventType.SessionEnd},
            {"StartActuatingFront", HBEventType.StartActuatingFront},
            {"StartActuatingBack", HBEventType.StartActuatingBack},
            {"StartActuatingRight", HBEventType.StartActuatingRight},
            {"StartActuatingLeft", HBEventType.StartActuatingLeft},
            {"StopActuating", HBEventType.StopActuating},
        };

    private static HBEventType ConvertStringToHBEventType(string inString)
    {
        return _hbEventTypeLookup[inString];
    }

    [Button]
    public void SaveToEuler()
    {
        var writer = new StreamWriter(eulerPath);
        writer.WriteLine("index,x,y,z");
        for (var index = 0; index < loadedIMUData.Count; index++)
        {
            var rot = loadedIMUData[index].quat;
            var eul = (transform.localRotation * rot).eulerAngles;
            writer.WriteLine($"{index},{eul.x:F6},{eul.y:F6},{eul.z:F6}");
        }

        writer.Close();
        writer.Dispose();
        Debug.Log("Saved to euler");
    }
    
    [Button(ButtonStyle.FoldoutButton)]
    public void SaveToAngleDifference(int preSampleAmount = 300)
    {
        SaveToAngleDifference(angleDifferencePath, fromToRange.x - preSampleAmount, fromToRange.x, fromToRange.x, fromToRange.y, loadedIMUData[fromToRange.x].time);
    }
    
    private void SaveToAngleDifference(string filePath, int sampleFrom, int sampleTo, int from, int to, float baseTime)
    {
        var vecResult = Vector3.zero;
        for (var i = sampleFrom; i < sampleTo; i++)
        {
            vecResult += loadedRightVectors[i];
        }

        vecResult = vecResult.normalized;
        
        var writer = new StreamWriter(filePath);
        writer.WriteLine("time,angle");
        for (var index = from; index < to; index++)
        {
            var angle = Vector3.Angle(vecResult, loadedRightVectors[index]);
            writer.WriteLine($"{loadedIMUData[index].time - baseTime:F6},{angle:F6}");
        }

        writer.Close();
        writer.Dispose();
        Debug.Log($"Saved to angle difference to {filePath}");
    }

    private readonly Dictionary<HBEventType, string> _directionLookup = new Dictionary<HBEventType, string>()
    {
        {HBEventType.StartActuatingFront, "front"},
        {HBEventType.StartActuatingBack, "back"},
        {HBEventType.StartActuatingRight, "right"},
        {HBEventType.StartActuatingLeft, "left"}
    };

    [Button]
    public void GenerateAngleDifferenceFiles()
    {
        for (var i = 0; ; i++)
        {
            var seatedDataPath = Path.Combine(quatVizBatchFolderPath, $"tester-{i}-seated-data.csv");
            var seatedEventPath = Path.Combine(quatVizBatchFolderPath, $"tester-{i}-seated-event.csv");
            if (File.Exists(seatedDataPath) && File.Exists(seatedEventPath))
            {
                GenerateAngleDifferenceFile(i, "seated", seatedDataPath, seatedEventPath);
            }
            else
            {
                Debug.Log($"Found and generated {i} angle difference file(s)");
                break;
            }
            
            var standingDataPath = Path.Combine(quatVizBatchFolderPath, $"tester-{i}-standing-data.csv");
            var standingEventPath = Path.Combine(quatVizBatchFolderPath, $"tester-{i}-standing-event.csv");
            if (File.Exists(standingDataPath) && File.Exists(standingEventPath))
            {
                GenerateAngleDifferenceFile(i, "standing", standingDataPath, standingEventPath);
            }
            else
            {
                Debug.Log($"Found and generated {i} angle difference file(s)");
                break;
            }
        }
    }

    private void GenerateAngleDifferenceFile(int testerId, string posture, string dataFilePath, string eventFilePath)
    {
        Load(dataFilePath, eventFilePath);
        
        var dataPoint = 0;
        for (var index = 0; index < loadedEventData.Count; index++)
        {
            var eventEntry = loadedEventData[index];
            if (eventEntry.type < HBEventType.StartActuatingFront
                || eventEntry.type > HBEventType.StartActuatingLeft) continue;

            var sampleFromTime = eventEntry.time - 1f;
            var fromTime = eventEntry.time - 0.5f;
            var sampleToTime = eventEntry.time;
            var toTime = loadedEventData[index + 1].time + 3;

            int fromIndex, toIndex, sampleFromIndex, sampleToIndex;
            while (loadedIMUData[dataPoint].time < sampleFromTime) dataPoint++;
            sampleFromIndex = dataPoint;
            while (loadedIMUData[dataPoint].time < fromTime) dataPoint++;
            fromIndex = dataPoint;
            while (loadedIMUData[dataPoint].time < sampleToTime) dataPoint++;
            sampleToIndex = dataPoint;
            while (dataPoint + 1 < loadedIMUData.Count && loadedIMUData[dataPoint].time < toTime) dataPoint++;
            toIndex = dataPoint;

            var targetPath = Path.Combine(generatedAngleDiffFolderPath,
                $"tester{testerId}-{posture}-{fromIndex:D5}-{_directionLookup[eventEntry.type]}.csv");
            
            SaveToAngleDifference(targetPath, sampleFromIndex, sampleToIndex, fromIndex, toIndex, eventEntry.time);
        }
    }

    private void OnDrawGizmos()
    {
        if (loadedRightVectors != null && loadedRightVectors.Count > 0)
        {
            var dataCount = fromToRange.y - fromToRange.x;
            var dataPerLine = Mathf.Max(dataCount / 300, 1);
            for (var i = fromToRange.x; i < fromToRange.y; i += dataPerLine)
            {
                var p1 = Vector3.zero;
                var p2 = transform.localRotation * loadedIMUData[i].quat * Vector3.forward;
                var color = Color.Lerp(Color.blue, Color.cyan, Mathf.InverseLerp(fromToRange.x, fromToRange.y, i));
                color.a = lineAlpha;
                
                Handles.DrawBezier(p1,p2,p1,p2, color,null, lineThickness);

                p2 = transform.localRotation * loadedIMUData[i].quat * Vector3.up;
                var upColor = Color.Lerp(Color.green, Color.yellow, Mathf.InverseLerp(fromToRange.x, fromToRange.y, i));
                upColor.a = lineAlpha;
                
                Handles.DrawBezier(p1,p2,p1,p2, upColor,null, lineThickness);
                
                p2 = transform.localRotation * loadedIMUData[i].quat * Vector3.right;
                var rightColor = Color.Lerp(Color.red, Color.magenta, Mathf.InverseLerp(fromToRange.x, fromToRange.y, i));
                rightColor.a = lineAlpha;
                
                Handles.DrawBezier(p1,p2,p1,p2, rightColor,null, lineThickness);
            }
        }
    }
}
