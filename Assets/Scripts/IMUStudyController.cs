﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Serialization;
using UnityEngine.Video;
using YoutubePlayer;
using Debug = UnityEngine.Debug;

namespace AirDriVR
{
    
    public class IMUStudyController : MonoBehaviour
    {
        public enum Direction
        {
            F, B, R, L
        }

        public YoutubePlayer.YoutubePlayer youtubePlayer;

        [FormerlySerializedAs("sittingVideoUrl")] public string seatedVideoUrl;
        public string standingVideoUrl;

        [ReadOnly][HideInEditorMode]
        public bool isStudyOngoing = false;
    
        [ReadOnly][HideInEditorMode]
        public bool isSeated = true;
    
        [ReadOnly][HideInEditorMode]
        public int testerId = 0;

        [ReadOnly][HideInEditorMode]
        public int currentTrial = 0;

        private Stopwatch timer;

        public event EventHandler onWriteEvent;
        public event EventHandler onSessionEnd;

        public string LastWriteEventDescription { get; set; }

        public float GetTime()
        {
            return timer != null ? (float)timer.Elapsed.TotalSeconds : 0;
        }

        public IMUDataReader imuDataReader;
        public IMUDataWriter imuDataWriter;
    
        public float actuateInterval = 3f;
    
        [ReadOnly]
        public float[] waitIntervals = {11f, 9f, 15f, 5f, 17f, 13f, 7f, 19f};

        public const int TrialCount = 8;
        public const int TesterCount = 12;
        
        public Direction[,] seatedLatinSquareOrders =
        {
            {Direction.F, Direction.R, Direction.L, Direction.B, Direction.R, Direction.F, Direction.B, Direction.L},
            {Direction.B, Direction.L, Direction.F, Direction.R, Direction.F, Direction.R, Direction.L, Direction.B},
            {Direction.R, Direction.F, Direction.B, Direction.L, Direction.L, Direction.B, Direction.R, Direction.F},
            {Direction.L, Direction.B, Direction.R, Direction.F, Direction.B, Direction.L, Direction.F, Direction.R},
            {Direction.F, Direction.R, Direction.L, Direction.B, Direction.R, Direction.F, Direction.B, Direction.L},
            {Direction.B, Direction.L, Direction.F, Direction.R, Direction.F, Direction.R, Direction.L, Direction.B},
            {Direction.R, Direction.F, Direction.B, Direction.L, Direction.L, Direction.B, Direction.R, Direction.F},
            {Direction.L, Direction.B, Direction.R, Direction.F, Direction.B, Direction.L, Direction.F, Direction.R},
            {Direction.F, Direction.R, Direction.L, Direction.B, Direction.R, Direction.F, Direction.B, Direction.L},
            {Direction.B, Direction.L, Direction.F, Direction.R, Direction.F, Direction.R, Direction.L, Direction.B},
            {Direction.R, Direction.F, Direction.B, Direction.L, Direction.L, Direction.B, Direction.R, Direction.F},
            {Direction.L, Direction.B, Direction.R, Direction.F, Direction.B, Direction.L, Direction.F, Direction.R},
        };
        
        public Direction[,] standingLatinSquareOrders =
        {
            {Direction.L, Direction.B, Direction.R, Direction.F, Direction.B, Direction.L, Direction.F, Direction.R},
            {Direction.F, Direction.R, Direction.L, Direction.B, Direction.R, Direction.F, Direction.B, Direction.L},
            {Direction.B, Direction.L, Direction.F, Direction.R, Direction.F, Direction.R, Direction.L, Direction.B},
            {Direction.R, Direction.F, Direction.B, Direction.L, Direction.L, Direction.B, Direction.R, Direction.F},
            {Direction.L, Direction.B, Direction.R, Direction.F, Direction.B, Direction.L, Direction.F, Direction.R},
            {Direction.F, Direction.R, Direction.L, Direction.B, Direction.R, Direction.F, Direction.B, Direction.L},
            {Direction.B, Direction.L, Direction.F, Direction.R, Direction.F, Direction.R, Direction.L, Direction.B},
            {Direction.R, Direction.F, Direction.B, Direction.L, Direction.L, Direction.B, Direction.R, Direction.F},
            {Direction.L, Direction.B, Direction.R, Direction.F, Direction.B, Direction.L, Direction.F, Direction.R},
            {Direction.F, Direction.R, Direction.L, Direction.B, Direction.R, Direction.F, Direction.B, Direction.L},
            {Direction.B, Direction.L, Direction.F, Direction.R, Direction.F, Direction.R, Direction.L, Direction.B},
            {Direction.R, Direction.F, Direction.B, Direction.L, Direction.L, Direction.B, Direction.R, Direction.F},
        };

        private void Start()
        {
            if (imuDataReader.ConnectToIMU())
            {
                imuDataReader.onDataPolled += OnReceivedReaderData;
            }
        }

        private void OnDisable()
        {
            imuDataReader.Disconnect();
            imuDataWriter.EndWrite();
        }

        public void BeginStudy()
        {
            isStudyOngoing = true;
            youtubePlayer.PlayVideoAsync((isSeated) ? seatedVideoUrl : standingVideoUrl);
            currentTrial = 0;
            
            timer = new Stopwatch();
            timer.Start();
            imuDataWriter.BeginWrite(testerId, isSeated);
            WriteEvent("SessionStart");
            StartCoroutine(WaitInterval());
        }

        private IEnumerator WaitInterval()
        {
            if (currentTrial >= TrialCount)
            {
                Debug.Log("All trials ended");
                yield return new WaitForSecondsRealtime(3f);
                OnSessionEnded();
                yield break;
            }
        
            yield return new WaitForSecondsRealtime(waitIntervals[currentTrial]);
            StartCoroutine(Actuate());
        }

        private IEnumerator Actuate()
        {
            // TODO: Connect to AirDriVR and actually actuate
            switch (isSeated ? seatedLatinSquareOrders[testerId, currentTrial] : standingLatinSquareOrders[testerId, currentTrial])
            {
                case Direction.F:
                    Debug.Log("Started actuating front");
                    WriteEvent("StartActuatingFront");
                    break;
                case Direction.B:
                    Debug.Log("Started actuating back");
                    WriteEvent("StartActuatingBack");
                    break;
                case Direction.R:
                    Debug.Log("Started actuating right");
                    WriteEvent("StartActuatingRight");
                    break;
                case Direction.L:
                    Debug.Log("Started actuating left");
                    WriteEvent("StartActuatingLeft");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        
            yield return new WaitForSecondsRealtime(actuateInterval);
        
            // TODO: Call AirDriVR to stop actuating
            Debug.Log("Stopped actuating");
            WriteEvent("StopActuating");

            currentTrial++;

            StartCoroutine(WaitInterval());
        }

        public void EndSession()
        {
            if (!isStudyOngoing) return;
            
            StopAllCoroutines();
            OnSessionEnded();
        }

        private void OnSessionEnded()
        {
            isStudyOngoing = false;
            youtubePlayer.GetComponent<VideoPlayer>().Stop();
            onSessionEnd?.Invoke(this, EventArgs.Empty);
            WriteEvent("SessionEnd");
            imuDataWriter.EndWrite();
            FindObjectOfType<RenderTextureClearer>()?.Clear();
        }

        private void OnReceivedReaderData(object reader, IMUPollDataEventArgs args)
        {
            if(imuDataWriter.IsWriting) imuDataWriter.WriteData(GetTime(), args.acceleration, args.orientation);
        }

        private void WriteEvent(string eventDescrtipion)
        {
            LastWriteEventDescription = eventDescrtipion;
            onWriteEvent?.Invoke(this, EventArgs.Empty);
            imuDataWriter.WriteEvent(GetTime(), eventDescrtipion);
        }
    }
}