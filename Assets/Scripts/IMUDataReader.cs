﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AirDriVR
{
    public class IMUPollDataEventArgs : EventArgs
    {
        public Vector3 acceleration;
        public Quaternion orientation;

        public IMUPollDataEventArgs(Vector3 acceleration, Quaternion orientation)
        {
            this.acceleration = acceleration;
            this.orientation = orientation;
        }
    }
    
    [Serializable]
    public class IMUDataReader
    {
        public bool debugIO = false;
        
        public string portName = "COM3";
        public int baudRate = 115200;
        private SerialPort port;

        public event EventHandler<IMUPollDataEventArgs> onDataPolled;

        private bool isPolling = false;

        private Vector3 acceleration;
        private Quaternion orientation;

        public bool ConnectToIMU()
        {
            if (port != null && port.IsOpen)
            {
                Debug.Log("Reader: Port already open; trying to disconnect and reconnect");
                Disconnect();
                return ConnectToIMU();
            }

            if (debugIO)
            {
                Debug.Log("Reader runs in debug IO mode");
                portName = "DebugIO";
                baudRate = 42069;
            }
            else
            {
                try
                {
                    port = new SerialPort(portName, baudRate);
                    port.Open();
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                    return false;
                }
            }
            
            Debug.Log($"Reader connected to {portName} at baud rate {baudRate}");

            isPolling = true;
            Task.Run(PollData);

            return true;
        }

        private void PollData()
        {
            Debug.Log("Reader started polling");
            while (isPolling)
            {
                if (debugIO)
                {
                    Thread.Sleep(1);
                    acceleration = new Vector3(42069, 123, 654);
                    orientation = new Quaternion(1, 2, 3, 4);
                }
                else
                {
                    // TODO: Read from Arduino board
                
                    if(port.BytesToRead <= 0) continue;
                }
                
                DispatchPollDataEvent();
            }
            Debug.Log("Reader stopped polling");
        }

        private void DispatchPollDataEvent()
        {
            onDataPolled?.Invoke(this, new IMUPollDataEventArgs(acceleration, orientation));
        }

        public void Disconnect()
        {
            onDataPolled = null;
            isPolling = false;
            Thread.Sleep(1); // Sleep to wait for PollData to end
            port?.Close();
            
            Debug.Log("Reader disconnected");
        }
    }
}