﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Sirenix.OdinInspector;
using UnityEngine;

namespace AirDriVR
{
    [Serializable]
    public class IMUDataWriter
    {
        [FolderPath]
        public string dataFolderPath;
        private StreamWriter dataWriter, eventWriter;

        public bool IsWriting { get; private set; } = false;

        public string seatedString = "seated";
        public string standingString = "standing";
            
        public void BeginWrite(int testerId, bool isSeated)
        {
            var commonPath = Path.Combine(dataFolderPath, $"tester-{testerId}-{(isSeated ? seatedString : standingString)}");
            var dataPath = commonPath + "-data.csv";
            var eventPath = commonPath + "-event.csv";
            
            if (File.Exists(dataPath) || File.Exists(eventPath))
            {
                throw new AccessViolationException("Tester data already exist");
            }
            
            dataWriter = new StreamWriter(dataPath);
            eventWriter = new StreamWriter(eventPath);
            dataWriter.WriteLine("time,accelX,accelY,accelZ,orientW,orientX,orientY,orientZ");
            eventWriter.WriteLine("time,eventDescription");

            IsWriting = true;
        }

        public void WriteData(float time, Vector3 acceleration, Quaternion orientation)
        {
            dataWriter?.WriteLine("{0:F6},{1:F},{2:F},{3:F},{4:F},{5:F},{6:F},{7:F}",
                time,
                acceleration.x,
                acceleration.y,
                acceleration.z,
                orientation.w,
                orientation.x,
                orientation.y,
                orientation.z);
        }

        public void WriteEvent(float time, string eventDescription)
        {
            eventWriter?.WriteLine($"{time:F6},{eventDescription}");
        }

        public void EndWrite()
        {
            IsWriting = false;
            
            dataWriter?.Dispose();
            eventWriter?.Dispose();
        }
    }
}